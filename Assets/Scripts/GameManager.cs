﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    public float playerSpeed = 10;
    public float bulletSpeed = 5;
    public int puntuation=0;
    private void Awake()    
    {
        if (_instance == null)
        {

            _instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void Reset()
    {
        SceneManager.LoadScene("M17UF1-Prova");
    }
    public void SumPunt(int points)
    {
        puntuation += points;
    }
}