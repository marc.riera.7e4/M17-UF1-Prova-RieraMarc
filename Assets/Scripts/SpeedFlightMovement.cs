﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFlightMovement : MonoBehaviour
{
    public float speed = 8;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.down * speed * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
       
            Destroy(gameObject);
        GameManager.Instance.SumPunt(5);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
