﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public GameObject bullet;
    private bool shoot = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space) && shoot == true)
        {
            GameObject bulletClone = Instantiate(bullet);
            bulletClone.transform.position = this.transform.position;
            bulletClone.transform.rotation = Quaternion.Euler(0, 0, 0);
            shoot = false;
            StartCoroutine(cd());
        }

    }
    IEnumerator cd()
    {
        yield return new WaitForSeconds(0.5f);
        shoot = true;
    }
}
    

