﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootFragate : MonoBehaviour
{
    private bool exit = false;
    public GameObject bullet;
    private void Start()
    {
        StartCoroutine(cd());
    }
    IEnumerator cd()
    {
        while (!exit)
        {
            yield return new WaitForSeconds(1.2f);
            GameObject bulletClone = Instantiate(bullet);
            bulletClone.transform.position = this.transform.position;
            bulletClone.transform.rotation = Quaternion.Euler(0, 0, 0);

        }
        
    }
}
