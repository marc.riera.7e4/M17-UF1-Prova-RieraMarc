﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateMovement : MonoBehaviour
{
    private int lifes = 5;
    public float speed = 1;
    void Update()
    {
        transform.position += Vector3.down * speed * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            lifes--;
            if (lifes <= 0) { Destroy(gameObject);
                GameManager.Instance.SumPunt(20);
            }
            

        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
