﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    private bool exit = false;
    public GameObject enemy1;
    public GameObject enemy2;
    void Start()
    {
        StartCoroutine(spawn());
    }

    IEnumerator spawn()
    {
        while (!exit)
        {
            yield return new WaitForSeconds(0.5f);
            GameObject enemyclone = Instantiate(enemy1);
            enemyclone.transform.position = new Vector3(Random.RandomRange(8.5f, -8.5f), this.transform.position.y, 0);
            enemyclone.transform.rotation = Quaternion.Euler(0, 0, 0);
            float spawn = 15 / 100;
            if (Random.value < spawn)
            {
                Instantiate(enemy2, new Vector3(Random.RandomRange(8.5f, -8.5f), this.transform.position.y, 0), Quaternion.identity);
            }
        }

    }

}
